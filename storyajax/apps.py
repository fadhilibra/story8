from django.apps import AppConfig


class StoryajaxConfig(AppConfig):
    name = 'storyajax'
