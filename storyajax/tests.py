from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .apps import *

# Create your tests here.
class story9UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_caribuku(self):
        found = resolve('/')
        self.assertEqual(found.func, caribuku)

    def test_apps(self):
        self.assertEqual(StoryajaxConfig.name, 'storyajax')
        self.assertEqual(apps.get_app_config('storyajax').name, 'storyajax')
