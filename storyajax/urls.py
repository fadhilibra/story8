from django.urls import path
from . import views

app_name = 'storyajax'

urlpatterns = [
    path('', views.caribuku, name='caribuku'),
    path('jsonreq/<str:book>', views.jsonreq_func, name='jsonreq_func'),
    
]
